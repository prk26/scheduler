const visualisation = ["#453064", "#5f64c0", "#91e4a6", "#449187"];

const appointments = {
    colour: "#000000",
    JobTask: "#baffc9",
    Leave: "#ffb3ba",
    Training: "#bae1ff",
    OutOfService: "#ffb3ba",
};

const theme = {
    colours: {
        appointments,
        primary: "#0e213e",
        primaryVariant: "#1c437d",
        onPrimary: "#E8ECF2",
        secondary: "#56dc9b",
        secondaryVariant: "#56dc9b",
        onSecondary: "#000000",
        background: "#ffffff",
        onBackground: "#000000",
        disabled: "#d9d9d9",
        onDisabled: "#9e9e9e",
        progress: "#004fb0",
        success: "#00b067",
        surface: "#ffffff",
        onSurface: "#000000",
        onSurfaceLight: "#ededed",
        error: "#E53935",
        onError: "#ffffff",
        visualisation,
    },
};

export default theme;
