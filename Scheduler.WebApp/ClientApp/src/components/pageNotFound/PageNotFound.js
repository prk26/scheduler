import React from "react";
import Container from "../common/containers";

const PageNotFound = () => {
    return (
        <Container>
            <h2>Page not found</h2>
        </Container>
    );
};

export default PageNotFound;
