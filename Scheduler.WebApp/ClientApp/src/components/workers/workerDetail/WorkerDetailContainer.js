import React, { useState } from "react";
import { Link } from "react-router-dom";
import { workersService } from "../../../services";
import { Loading, LoadingFailure } from "../../common/loading";
import Breadcrumb from "../../common/breadcrumb";
import Container from "../../common/containers";
import WorkerDetail from "./WorkerDetail";
import Routes from "../../../routes";
import { useWorkerCalendar } from "../workersSchedule/workersCalendar";
import Alert from "../../common/alert";

const WorkerDetailContainer = props => {
    const id = props.match.params.id;
    const [loading, loadingError, worker] = useWorkerCalendar(id);
    const [deleteError, setDeleteError] = useState();

    const handleDelete = () => {
        setDeleteError(null);
        workersService
            .destroy(id)
            .then(() => props.history.push(Routes.workers.LIST))
            .catch(error => setDeleteError(error));
    };

    const renderBreadcrumb = () => (
        <Breadcrumb>
            <Link className="breadcrumb-item" to={Routes.workers.LIST}>
                Staff
            </Link>
            <Breadcrumb.Item active>{worker.name}</Breadcrumb.Item>
        </Breadcrumb>
    );

    const renderComponent = component => <Container>{component}</Container>;

    if (loading) return renderComponent(<Loading />);
    if (loadingError)
        return renderComponent(
            <LoadingFailure message={loadingError.message} />
        );

    return renderComponent(
        <>
            {renderBreadcrumb()}
            {deleteError && (
                <Alert
                    className="mt-4"
                    variant="danger"
                    dismissible
                    onClose={() => setDeleteError(null)}
                >
                    Delete failed: {deleteError.message}
                </Alert>
            )}
            <WorkerDetail worker={worker} handleDelete={handleDelete} />
        </>
    );
};

export default WorkerDetailContainer;
