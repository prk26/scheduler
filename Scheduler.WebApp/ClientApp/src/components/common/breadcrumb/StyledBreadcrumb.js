import styled from "styled-components";
import Breadcrumb from "react-bootstrap/Breadcrumb";

const StyledBreadcrumb = styled(Breadcrumb)`
    margin-top: 20px;

    ol {
        background: none;
        border-radius: 0;
        padding: 0;
    }
`;

export default StyledBreadcrumb;
