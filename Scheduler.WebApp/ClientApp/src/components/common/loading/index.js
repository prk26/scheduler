import Loading from "./Loading";
import LoadingFailure from "./LoadingFailure";

export { Loading, LoadingFailure };
