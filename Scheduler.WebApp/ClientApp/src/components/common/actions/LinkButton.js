import styled from "styled-components";

const LinkButton = styled.button`
    background: none;
    border: none;

    &:hover {
        text-decoration: underline;
    }
`;

export default LinkButton;
