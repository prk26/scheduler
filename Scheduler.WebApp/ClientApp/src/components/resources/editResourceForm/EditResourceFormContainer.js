import React from "react";
import EditResourceForm from "./EditResourceForm";
import Alert from "../../common/alert";
import Container from "../../common/containers";
import Breadcrumb from "../../common/breadcrumb";
import { Loading, LoadingFailure } from "../../common/loading";
import { isEqual } from "lodash";
import Routes from "../../../routes";
import { Link, generatePath } from "react-router-dom";
import { resourcesService } from "../../../services";

class EditResourceFormContainer extends React.Component {
    state = {
        loading: true,
        resource: null,
        loadingError: null,
        formError: null,
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        resourcesService
            .getById(id)
            .then(resource => this.setState({ resource }))
            .catch(error => this.setState({ loadingError: error }))
            .finally(() => this.setState({ loading: false }));
    }

    handleCancel = () => this.props.history.goBack();

    handleSubmit = (values, { setSubmitting }) => {
        this.setState({ formError: null });

        const { resource } = this.state;
        if (isEqual(values, resource)) {
            this.setState({ formError: { message: "No changes made." } });
            setSubmitting(false);
            return;
        }
        resourcesService
            .edit(values)
            .then(() => {
                const resourceDetailPath = generatePath(
                    Routes.resources.DETAIL,
                    { id: resource.id }
                );
                this.props.history.push(resourceDetailPath);
            })
            .catch(error => {
                this.setState({ formError: error });
                setSubmitting(false);
            });
    };

    renderBreadcrumb(resource) {
        const resourcePath = generatePath(Routes.resources.DETAIL, {
            id: resource.id,
        });
        return (
            <Breadcrumb>
                <Link className="breadcrumb-item" to={Routes.resources.LIST}>
                    Plant
                </Link>
                <Link className="breadcrumb-item" to={resourcePath}>
                    {resource.name}
                </Link>
                <Breadcrumb.Item active>Edit</Breadcrumb.Item>
            </Breadcrumb>
        );
    }

    renderComponent(component) {
        return (
            <Container>
                <h2>Edit Plant</h2>
                {component}
            </Container>
        );
    }

    render() {
        const { loading, loadingError, formError, resource } = this.state;
        if (loading) return this.renderComponent(<Loading />);
        if (loadingError)
            return this.renderComponent(
                <LoadingFailure message={loadingError.message} />
            );

        return (
            <Container>
                {this.renderBreadcrumb(resource)}
                <h2>Edit Plant</h2>
                {formError && (
                    <Alert variant="danger">{formError.message}</Alert>
                )}
                <EditResourceForm
                    resource={resource}
                    handleSubmit={this.handleSubmit}
                    handleCancel={this.handleCancel}
                />
            </Container>
        );
    }
}

export default EditResourceFormContainer;
